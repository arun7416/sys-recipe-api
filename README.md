#Overview

sys-recipe-api is proxy service of Food2Fork API. Food2Fork offers an API which exposes its powerful recipe discovery functions for your app to use. 
The API gives you access to our ever expanding recipe database, powerful ingredient search function, and social-media based ranking algorithm.


#Documentation

---
## API Console

Below is the API Console URL. API console will give the overview of the API and option to try calling the services exposed

http://arun-sys-recipe-api.us-e2.cloudhub.io/console/



## Submitting a Query

All search requests should be made to the search API URL.


http://arun-sys-recipe-api.us-e2.cloudhub.io/api/v1/recepies/search


All recipe requests should be made to the recipe details API URL.


http://arun-sys-recipe-api.us-e2.cloudhub.io/api/v1/recepies/get



##Authentication

This API is secured with client id enforcement policy. All request should contain below **headers** with proper values


```properties
client_id: Client ID obtained from the Mulesoft Anypoint platform for application access

client_secret: Client Secret obtained from the Mulesoft Anypoint platform for application access
```



## Parameters

All parameters can be encoded as either HTTP GET or POST parameters.


**Search Parameters**


  
```properties
key: (optional) API Key

query: (optional) Search Query (Ingredients should be separated by commas). If this is omitted top rated recipes will be returned.

sort: (optional) How the results should be sorted. See Below for details.

page: (optional) Used to get additional results
```


Below is the example url for searching recipes with "chicken breast" as an ingredient.


http://arun-sys-recipe-api.us-e2.cloudhub.io/api/v1/recepies/search?query=chicken%20breast&page=2



**Recipe Details Parameters**


  
```properties
key: (optional) API Key

recipeId: Id of desired recipe as returned by Search Query
```


Below is the example url for getting detailes of recipe with id 35382.


http://arun-sys-recipe-api.us-e2.cloudhub.io/api/v1/recepies/get?recipeId=35382



**Search Sorting**


The API is offers two kinds of sorting for queries. The first is by rating. This rating is based off of social media scores to determine the best recipes.

**sort=r**


The second is by trendingness. The most recent recipes from our publishers have a trend score based on how quickly they are gaining popularity.

**sort=t**



**Pages (Search Only)**


Any request will return a maximum of 30 results. To get the next set of results send the same request again but with **page = 2**


The default if omitted is **page = 1**



**Response Parameters**


The response is json encoded.


**Search**


Below is the structure of response


```properties
count: Number of recipes in result (Max 30)

recipes: List of Recipe Parameters ->

	image_url: URL of the image
	
	source_url: Original Url of the recipe on the publisher's site
	
	f2f_url: Url of the recipe on Food2Fork.com
	
	title: Title of the recipe
	
	publisher: Name of the Publisher
	
	publisher_url: Base url of the publisher
	
	social_rank: The Social Ranking of the Recipe (As determined by our Ranking Algorithm)
	
	page: The page number that is being returned (To keep track of concurrent requests)
```

**Sample Response**


Request: http://arun-sys-recipe-api.us-e2.cloudhub.io/api/v1/recepies/search?query=shredded%20chicken


```json
{
	"count": 1, 
	"recipes": [
		{
			"publisher": "Allrecipes.com",
			"social_rank": 99.81007979198002, 
			"f2f_url": "https://www.food2fork.com/recipes/view/29159", 
			"publisher_url": "http://allrecipes.com", 
			"title": "Slow-Cooker Chicken Tortilla Soup", 
			"source_url": "http://allrecipes.com/Recipe/Slow-Cooker-Chicken-Tortilla-Soup/Detail.aspx",
			"page":1
		}
	]
}
```


**Get Recipe**


Below is the structure of response


```
recipe: List of Recipe Parameters ->

	image_url: URL of the image
	
	source_url: Original Url of the recipe on the publisher's site
	
	f2f_url: Url of the recipe on Food2Fork.com
	
	title: Title of the recipe
	
	publisher: Name of the Publisher
	
	publisher_url: Base url of the publisher
	
	social_rank: The Social Ranking of the Recipe (As determined by our Ranking Algorithm)
	
	ingredients: The ingredients of this recipe
```


**Sample Response**


Request: http://arun-sys-recipe-api.us-e2.cloudhub.io/api/v1/recepies/get?recipeId=948c95


Below is the structure of response


```json
{
   "recipe": {
      "publisher": "Big Girls Small Kitchen",
      "f2f_url": "http://food2fork.com/view/948c95",
      "ingredients": [
         "1/2 pound chorizo",
         "1 pound boneless skinless chicken thighs",
         "4 tablespoons butter",
         "1 small onion, diced",
         "3 cloves of garlic, minced",
         "3 teaspoons ancho chile powder (or chili powder)",
         "1 teaspoon cumin",
         "1 teaspoon salt",
         "2 tablespoons flour",
         "1 cup chicken broth",
         "1/2 cup cream",
         "1/2 cup sour cream",
         "1/2 lime, juiced",
         "3 cups cooked long grain white rice",
         "1/2 cup cilantro leaves, chopped",
         "3 cups coarsely shredded sharp cheddar cheese"
      ],
      "source_url": "http://www.biggirlssmallkitchen.com/2011/04/big-girls-test-kitchen-100-calorie-casserole.html",
      "recipe_id": "948c95",
      "image_url": "http://static.food2fork.com/IMG_671460f4.jpg",
      "social_rank": 95.54755,
      "publisher_url": "http://www.biggirlssmallkitchen.com/",
      "title": "Big Girls, Test Kitchen: 100 Calorie Casserole Creamy Chorizo-Chicken Casserole"
   }
}
```

---
